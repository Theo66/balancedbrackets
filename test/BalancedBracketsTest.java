/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bazooka
 */
public class BalancedBracketsTest {
    
    public BalancedBracketsTest() {
    }

    /**
     * Test of isBalanced method, of class BalancedBrackets.
     */
    @Test
    public void testIsBalanced() {
        BalancedBrackets mytest = new BalancedBrackets();
        String test;
        
        test = "{[()]}";
        assertEquals(true, mytest.isBalanced(test));
        
        test = "{[()]}{}[()]";
        assertEquals(true, mytest.isBalanced(test));
        
        test = "{[()]}{(())}[([[]])]";
        assertEquals(true, mytest.isBalanced(test));
        
        test = "{[()]";
        assertEquals(false, mytest.isBalanced(test));
        
        test = "{{";
        assertEquals(false, mytest.isBalanced(test));
        
        test = "{";
        assertEquals(false, mytest.isBalanced(test));
        
        test = "{[()]}}";
        assertEquals(false, mytest.isBalanced(test));
        
        test = "{[()]}}}";
        assertEquals(false, mytest.isBalanced(test));
        
        test = "]]";
        assertEquals(false, mytest.isBalanced(test));
        
        test = "]";
        assertEquals(false, mytest.isBalanced(test));
        
        test = "";
        assertEquals(false, mytest.isBalanced(test));
    }
    
}
