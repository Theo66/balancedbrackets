The task was completed using Netbeans 8.2.
Simply import the project, compile and run.

The program uses HashMap for the bracket pairs, mapping each opening bracket(key), 
with its respective closing bracket(value).

Checking the string character by character, each opening bracket is inserted in a stack.

While checking the string, 
if a symbol is NOT an opening bracket we pop an element from the stack,
and the popped element should be the pair to that previous symbol, 
IF we have a balanced string.

So popped element should be the Map Key and the checked symbol should be its' Map Value. 
If these two don't have such a relationship, then string isn't balanced.

Trying to pop an empty stack, or a stack being non-empty after checking the whole string
also indicates a non balanced string.

###########################################################################################

Instructions:
The program receives inputs from the user and while a user input is a string of balanced brackets,
it prints true to the console. 
For any other string of either non-balanced brackets or random symbols, it prints false.
The program exits when it receives an empty input.

Unit Tests are also provided inside the "Test packages" of the project.



