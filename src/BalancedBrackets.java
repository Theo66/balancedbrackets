
import java.util.*;


public class BalancedBrackets {

    public boolean isBalanced(String s) {
        if ( s.length()%2!=0 || s.length()==0 ) return false; //for starters, a string that is not an even number of symbols, OR is empty, can't be balanced
  
        Map<Character,Character> br_pairs = new HashMap<Character,Character>(); //map of brackets, where opener brackets (key) are paired to corresponding closing brackets(value)
        br_pairs.put('[',']');
        br_pairs.put('{','}');
        br_pairs.put('(',')');
        
        Stack<Character> br_stack=new Stack<Character>(); // hold Opening brackets only      
        
        for(int i=0; i<s.length(); i++){
            if(br_pairs.get(s.charAt(i))!=null){  //is this an opening bracket?
                br_stack.push(s.charAt(i));     //if so, add it to the stack
            }
            else if(  br_stack.isEmpty()    ||    (br_pairs.get(br_stack.pop())!=s.charAt(i))  ) return false;  //
             //If the stack is empty when we try to pop, OR the element we popped is not pair with current element of string, then string is not balanced.                  
        }
        
        if(br_stack.isEmpty()==false) return false; // we shouldn't have left-overs in the end when a string is balanced
        
        return true;  // if none of the above conditions are met, then our string was balanced  
    }

    public static void main(String[] args) {

        BalancedBrackets sample=new BalancedBrackets();
        
        Scanner input = new Scanner(System.in);  
        String s=input.nextLine();
        
        while (!s.isEmpty()){
            System.out.println(sample.isBalanced(s));
            s = input.nextLine();
        } 
        System.out.println("Byeee");
        input.close();
    }
}
